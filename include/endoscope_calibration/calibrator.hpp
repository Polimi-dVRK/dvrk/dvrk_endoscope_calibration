//
// Created by tibo on 12/07/17.
//

#pragma once

#include <ros/ros.h>
#include <sensor_msgs/Joy.h>

#include <opencv2/core/types.hpp>

#include <dvrk_common/ros/camera/stereo_camera.hpp>

enum class ButtonState { Released= 0, Pressed = 1 };

/**
 * THe #Calibrator class is a convenient place to put al the data and functions required to calibrate the stereo
 * separation of the endoscope images.
 *
 * The idea behind the calibration procedure is that whilst the cameras record PAL images, so 576x720 pixels, the
 * monitors on the davinci console only display 640x480 pixels. instead of resizing the image we can use the extra
 * available pixels to virtually move the images from the left and right cameras closer or further apart.
 *
 * We achieve this be selecting a region of interest of 640x480 pixels initially centered on the camera images that can
 * be moved left or right by using the \c CLUTCH and \c COAG footpedals and moved up and down with the \c CAMERA_PLUS
 * and \c CAMERA_MINUS pedals. Every time time the ROI moves a message is printed to the console displaying the new
 * values.
 *
 * The requirements for this node to run are that the camera driver exposes an interface to set the region of interest
 * for the camera images. Here at Politecnico di Milano we use DeckLink BlackMagic cards for real time video acquisition
 * and developed a ROS camera driver for them that support ROI settings directly. If you camera driver does not have
 * this feature, the ROS \c img_proc nodes offer similar functionality. Specifically, the ROI is sent tot the camera
 * driver as a \c sensor_msgs::RegionOfInterest message. In roder to move the ROI around the footpedals must be
 * connected and working.
 *
 * Note that any changes to the region of interest will not be persisted by the tool. it is your responsibility to set
 * the region of interest each time ROS starts. After each change to the region of interest you will also have to
 * recalibrate the intrinsics of the camera.
 */
class Calibrator {
public:
    Calibrator();

    void run();

public: /* Callbacks */
    void OnCameraPlus(const sensor_msgs::Joy& msg);
    void OnCameraMinus(const sensor_msgs::Joy& msg);
    void OnCoag(const sensor_msgs::Joy& msg);
    void OnClutch(const sensor_msgs::Joy& msg);

private: /* Methods */
    
    void setROIParam(const cv::Rect2d& left_roi, const cv::Rect2d& right_roi);
    
private:
    ros::NodeHandle _node;
    ros::NodeHandle _pnode;

    /// Subscriber listening for changes on the \c CAMERA_PLUS pedal
    ros::Subscriber _camera_plus_subscriber;
    /// State of the \c CAMERA_PLUS pedal. This is updated by the subscriber.
    ButtonState _camera_plus_state = ButtonState::Released;

    /// Subscriber listening for changes on the \c CAMERA_MINUS pedal
    ros::Subscriber _camera_minus_subscriber;
    /// State of the \c CAMERA_MINUS pedal. This is updated by the subscriber.
    ButtonState _camera_minus_state = ButtonState::Released;

    /// Subscriber listening for changes on the \c COAG pedal
    ros::Subscriber _coag_subscriber;
    /// State of the \c COAG pedal. This is updated by the subscriber.
    ButtonState _coag_state = ButtonState::Released;

    /// Subscriber listening for changes on the \c CLUTCH pedal
    ros::Subscriber _clutch_subscriber;
    /// State of the \c CLUTCH pedal. This is updated by the subscriber.
    ButtonState _clutch_state = ButtonState::Released;

    /// A wrapper around the \c image_transport topic transporting images from the left camera
    dvrk::StereoCamera _stereo_camera;

    /**
     * The namespace containing the ROI params for the left image.
     *
     * This is usually the namespace of the image_proc/crop_decimate nodelet that is responsible for setting the region
     * of interest on the images. If you don't know what the namespace is look for "decimation_x", "height", "width",
     * "x_offset" or "y_offset" in the output of "rosparam list".
     */
    std::string _left_roi_param_namespace;
    
    /// The namespace containing the ROI params for the left image
    std::string _right_roi_param_namespace;
    
    // The size of the region of interest as specified in the launch file
    cv::Point2i _roi_size = {0, 0};
};

