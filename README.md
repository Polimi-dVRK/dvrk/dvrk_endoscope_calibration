# dvrk_endoscope_calibration

The endoscope calibration package contains a set of ROS nodes that
help to calibrate the stereo separation of two camera images. These
nodes were developed for use on a decomissioned Intuitive Surgical
daVinci robot using the dVRK software but could work with any stereo
endoscope.

The basic idea behind the nodes is that our endoscope cameras capture
images in a glorious PAL resolution of 576x720 pixels. However the
monitors on our system are operating at a resolution of 640x480 pixels.
Instead of downsizing our image we instead select a region of interest
(ROI) in the centre of the image.

By using the footpedals of the system the user can move this ROI left
and right and up and down. At a certain point the image should appear to
be 3D. Since the separation of the stereo images is entirely created in
software these nodes could be adapted for use on a wide variety of
systems.

### endoscope_calibration node

This node manages the footpedals and takes care of moving the ROI in
reaction to the inputs. I does not however show any images directly.
This is the job of the `console_preview` node.

Note: in order to apply the ROI the node simply sends a
`sensor_msgs::RegionOfInterest` to a user specified topic. We use a
DeckLink BlackMagic video acquisition card to read the images and
developed a ROS camera driver for it
([decklink_ros](https://gitlab.com/Polimi-dVRK/decklink/decklink_ros))
that supports changing the ROI on the fly. It seems that ROS `img_proc`
nodes allow for this too.

### Console Preview

The `console_preview` node creates a window that shows the current camera image. The images are read from the `image_raw` topic and should be appropriately remapped (or the node hoisted to the correct namespace). 

The node will respond to the following key presses: 

| Key | Action                                  |
| --- | --------------------------------------- |
|  F  | Toggle fullscreen                       |
|  R  | Remove the red channel from the image   |
|  G  | Remove the green channel from the image |
|  B  | Remove the blue channel from the image  |
| ESC | Exit                                    |


### Anaglyph Preview

The `anaglyph_preview` node generates a Red-Blue 3D view fusing be fusing the left and right camera images. This node is helpful to view alignment issues between the two cameras. 

#### Usage

The node listens for images on the `left/image_raw` and `right/image_raw` topics which should be appropriately remapped. It will open a window with the image and respond to the following keys: 

| Key | Action                 |
| --- | ---------------------- |
|  F  | Toggle fullscreen      |
|  L  | Show left image only   |
|  R  | Show right image only  |
|  A  | Show anaglyph 3D image |
| ESC | Exit                   |

### Dependencies

All of these nodes depend on the
[dvrk_common](https://gitlab.com/Polimi-dVRK/dvrk/dvrk_common) package
developped here at Nearlab.

To understand the parameters that should be passed to the nodes in order
to run them take a look at the files in the `launch/` directory.
