
/**
 * \file
 *
 * Calibrate the separation of the stereo images from the endoscope cameras.
 *
 * For a more complete description see \sa Calibrator.
 */
#include "endoscope_calibration/calibrator.hpp"

int main(int argc, char** argv) {

    ros::init(argc, argv, "endoscope_calibration");
    Calibrator calib;

    calib.run();

    return 0;
}
