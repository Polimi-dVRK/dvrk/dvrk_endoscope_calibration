
/**
 * \file
 *
 * This file implements a simple ROS node that can read a pair of \c image_transport topics and produces a visualisation
 * in an OpenCV preview window with the green channel zeroed in the left image and the blue channel zeroed in the right
 * channel.
 *
 * The only required parametera are \c left_image_topic and \c right_image_topic, the names of the topic to connect to
 * and from which the images from the left and righ cameras should be read.
 *
 * This node is particularly useful to highlight the differences between the left and right camera images. This was
 * particularly useful to us a Politecnico di Milano to debug parallelism issues on the endoscope cameras.
 */

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include <opencv2/highgui.hpp>

#include <dvrk_common/ros/params.hpp>
#include <dvrk_common/ros/camera/stereo_camera.hpp>

#include <dvrk_common/opencv/util.hpp>
#include <dvrk_common/opencv/highgui.hpp>

const std::string preview_window("Anglyph 3D (Red/Blue) View");

enum class ViewerMode {
    LeftImage, RightImage, AnaglyphImage
};

ViewerMode viewer_mode = ViewerMode::AnaglyphImage;

void stereo_pair_callback(
    const sensor_msgs::ImageConstPtr& left,
    const sensor_msgs::ImageConstPtr& right,
    const image_geometry::StereoCameraModel&
) {
    ROS_ASSERT(left->encoding == sensor_msgs::image_encodings::RGB8);
    ROS_ASSERT(right->encoding == sensor_msgs::image_encodings::RGB8);
    
    auto left_image = cv_bridge::toCvCopy(left, sensor_msgs::image_encodings::BGR8);
    auto right_image = cv_bridge::toCvCopy(right, sensor_msgs::image_encodings::BGR8);
    
    switch (viewer_mode) {
        case ViewerMode::LeftImage:
            cv::putText(
                left_image->image, "Showing: Left Image", {10, 40},
                cv::HersheyFonts::FONT_HERSHEY_DUPLEX, 1.0, {0, 252, 124}
            );
            cv::imshow(preview_window, left_image->image);
            break;
    
        case ViewerMode::RightImage:
            cv::putText(
                right_image->image, "Showing: Right Image", {10, 40},
                cv::HersheyFonts::FONT_HERSHEY_DUPLEX, 1.0, {0, 252, 124}
            );
            cv::imshow(preview_window, right_image->image);
            break;
    
        case ViewerMode::AnaglyphImage:
            cv::Mat left_image_channels[3], right_image_channels[3];
            cv::split(left_image->image, left_image_channels);
            cv::split(right_image->image, right_image_channels);
        
            cv::Mat anaglyph_image;
            cv::Mat anaglyph_image_channels[3] = {
                /* Blue  */ right_image_channels[0],
                /* Green */ right_image_channels[1],
                /* Red   */ left_image_channels[2]
            };
            cv::merge(anaglyph_image_channels, 3, anaglyph_image);
        
            cv::putText(
                anaglyph_image, "Showing: Anaglyph Image", {10, 40},
                cv::HersheyFonts::FONT_HERSHEY_DUPLEX, 1.0, {0, 252, 124}
            );
            
            cv::imshow(preview_window, anaglyph_image);
            break;
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "anaglyph_preview");

    ros::NodeHandle node;
    dvrk::StereoCamera camera(node, "left/image_raw", "right/image_raw");
    camera.setOnImageCallback(stereo_pair_callback);
    
    dvrk::AdvertisementChecker checker(node);
    checker.check({ "left/image_raw", "right/image_raw" });
    
    cv::namedWindow(preview_window, cv::WINDOW_NORMAL);
    
    while (ros::ok()) {
        ros::spinOnce();
        const auto keycode = dvrk::waitKey(static_cast<int>(1000 / 25.0f));
        
        switch (keycode) {
            case dvrk::KeyCode::F:
                dvrk::toggleFullScreen(preview_window);
                break;
                
            case dvrk::KeyCode::R:
                viewer_mode = ViewerMode::RightImage;
                break;
    
            case dvrk::KeyCode::L:
                viewer_mode = ViewerMode::LeftImage;
                break;
    
            case dvrk::KeyCode::A:
                viewer_mode = ViewerMode::AnaglyphImage;
                break;
                
            case dvrk::KeyCode::Escape:
                ros::shutdown();
                break;
                
            default: break;
        }
    }

    return 0;
}
