
/**
 * \file
 *
 * This file implements a simple ROS node that can show an \c image_transport::ImageTransport compatible topic in an
 * OpenCV preview window. The only required parameter is \c image_topic, the name of the topic to connect to and from
 * which the images should be read.
 *
 * Additionally the node can be used to zero out one channel of the input. This functionality however assumes that the
 * images are sent as BGRA but extending it to support other image formats is quite trivial. This functionality is quite
 * useful to highlight differences between the left and right images in a stereo viewer.
 */

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include <opencv2/highgui/highgui.hpp>

#include <dvrk_common/ros/params.hpp>
#include <dvrk_common/ros/camera/simple_camera.hpp>

#include <dvrk_common/opencv/util.hpp>
#include <dvrk_common/opencv/highgui.hpp>

bool strip_channel_r = false;
bool strip_channel_g = false;
bool strip_channel_b = false;

std::string preview_window = "Preview";


void on_image(
    const sensor_msgs::ImageConstPtr& img,
    const image_geometry::PinholeCameraModel&
) {
    ROS_ASSERT(img->encoding == sensor_msgs::image_encodings::RGB8);
    auto cv_image = cv_bridge::toCvCopy(img, "bgr8");
    
    if (!strip_channel_r && !strip_channel_b && !strip_channel_g) {
        cv::imshow(preview_window, cv_image->image);
        return;
    }
    
    cv::Mat channels[3];
    cv::split(cv_image->image, channels);
    
    cv::Mat final_channels[3] = {
        channels[0], channels[1], channels[2]
    };
    
    cv::Mat empty_channel(cv_image->image.size(), CV_8UC1, cv::Scalar(0));
    
    if (strip_channel_b)
        final_channels[0] = empty_channel;
    if (strip_channel_g)
        final_channels[1] = empty_channel;
    if (strip_channel_r)
        final_channels[2] = empty_channel;
    
    cv::Mat final_image;
    cv::merge(final_channels, 3, final_image);
    
    cv::imshow(preview_window, final_image);
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "console_preview", ros::init_options::AnonymousName);

    ros::NodeHandle node, pnode("~");
    dvrk::SimpleCamera camera(node, "image_raw", on_image);
    
    dvrk::AdvertisementChecker checker(node);
    checker.check({ "image_raw" });
    
    // Make sure that the window has a unique name
    preview_window += " - " + camera.getTopic();
    
    std::string strip_colour;
    if (pnode.getParam("strip_channels", strip_colour)) {
        // Assume bgr images
        for (char channel: strip_colour)
        switch (strip_colour.at(0)) {
        case 'B':
        case 'b':
            strip_channel_b = true;
            break;

        case 'G':
        case 'g':
            strip_channel_g = true;
            break;

        case 'R':
        case 'r':
            strip_channel_r = true;
            break;

        default:
            ROS_ERROR("Channel to be stripped is <%c> not one of 'R', 'G' or 'B'", channel);
            ros::shutdown();
        }
    }
    
    cv::namedWindow(preview_window, cv::WINDOW_NORMAL);
    
    while (ros::ok()) {
        ros::spinOnce();
        // const auto keycode = dvrk::waitKey(static_cast<int>(1000 / 25.0f));
    
        const int keypress = cv::waitKey(static_cast<int>(1000 / 25.0f));
        if (keypress != -1)
            ROS_INFO("Keypress: %i", keypress);
        
        const auto keycode = dvrk::make_key(keypress);
        
        switch (keycode) {
            case dvrk::KeyCode::F:
                dvrk::toggleFullScreen(preview_window);
                break;
        
            case dvrk::KeyCode::B:
                strip_channel_b = !strip_channel_b;
                ROS_INFO("B = %i, G = %i, R = %i", strip_channel_b, strip_channel_g, strip_channel_r);
                break;
        
            case dvrk::KeyCode::G:
                strip_channel_g = !strip_channel_g;
                ROS_INFO("B = %i, G = %i, R = %i", strip_channel_b, strip_channel_g, strip_channel_r);
                break;
        
            case dvrk::KeyCode::R:
                strip_channel_r = !strip_channel_r;
                ROS_INFO("B = %i, G = %i, R = %i", strip_channel_b, strip_channel_g, strip_channel_r);
                break;
        
            case dvrk::KeyCode::Escape:
                ros::shutdown();
                break;
        
            default: break;
        }
    }

    return 0;
}
