/**
 * \file
 *
 * The dvrk::StereoImage class uses a \c message_filters::ApproximateTime policy to synchronise the images. This has one
 * major default however: by default it will happily match the wrong image frames. It might for example match one frame
 * from the left camera with the next frame from the right camera. This can cause accuracy issues during the stereo
 * reconstruction. 
 *
 * This node implements a simple way of checking for this. If the images of a StereoPair are more than \c
 * maximum_interval seconds apart a warning will be shown. This value can be passed in as a parameter in the launch
 * file.
 *
 * To decide on a reasonable value for \c maximum_interval follow the following procedure:
 *
 * Assuming that images are coming at 25Hz, we have: 1/25 = 40ms per image.
 * The delay between the camera images will only be an issue if an image from one camera is more than 20ms out of sync
 * with the other.
 * In this case the matcher will think it corresponds to the next image on the other camera and for that frame the
 * stereo reconstruction error will go back.
 *
 * Just to be sure we put the maximum delay at half of this value, i.e. 10ms.
 *
 * The intended workflow with this node is to first run it without specifying the `maximum_interval` in the `roslaunch`.
 * This should cause the node to warn you if the image frames are not being correctly paired. If you see no ouput then
 * either the node is not receiving images or the iamges are all perfectly paired.
 *
 * If any warning appear then you must set the `maximum_interval`. Re-run the node with the value set. If no warning
 * appear then you have solved the problem.
 *
 * Remember: every time you use the \c StereoCamera in your own code you must call \c setMaxImageInterval and set it to
 * the `maximum_interval` value set in the launch file. This node only helps you identify a good value. it does not set
 * it for future use.
 *
 */

#include <cmath>
#include <dvrk_common/ros/params.hpp>
#include <dvrk_common/ros/camera/stereo_camera.hpp>

int main(int argc, char** argv) {

    ros::init(argc, argv, "log_lr_delay");

    ros::NodeHandle node;
    ros::NodeHandle pnode("~");

    auto left_image_topic = dvrk::getRequiredParam<std::string>(pnode, "left_image_topic");
    auto right_image_topic = dvrk::getRequiredParam<std::string>(pnode, "right_image_topic");
    dvrk::StereoCamera camera(node, left_image_topic, right_image_topic);

    double camera_framerate = dvrk::getRequiredParam<double>(pnode, "camera_framerate");

    // This is the maximum amount of delay that will not cause issues.
    double maximum_acceptable_interval = (1.0 / camera_framerate) / 2.0;
    ROS_INFO("Computed maximum acceptable interval is: %f", maximum_acceptable_interval);

    // This is a "safe" value of the maximum interval
    double maximum_allowed_interval = -1.0;
    if (pnode.getParam("maximum_interval", maximum_allowed_interval)) {
        camera.setMaxImageInterval(ros::Duration(maximum_allowed_interval));
        ROS_INFO("Set maximum image interval to %f", maximum_allowed_interval);
    }

    camera.setOnImageCallback(
      [&maximum_acceptable_interval](
          const sensor_msgs::ImageConstPtr& left,
              const sensor_msgs::ImageConstPtr& right,
              const image_geometry::StereoCameraModel&
          ) {
              const auto arrival_time_diff = std::fabs(left->header.stamp.toSec() - right->header.stamp.toSec());
             ROS_DEBUG("Frame interval: %f", arrival_time_diff);
            
              if (arrival_time_diff > maximum_acceptable_interval) {
                  ROS_WARN(
                      "Arrival times of L/R images: [%.3f, %.3f], Seq: [%d, %d], Interval: %.3f",
                      left->header.stamp.toSec(), right->header.stamp.toSec(),
                      left->header.seq, right->header.seq,
                      arrival_time_diff           
                  );  
              }
          }
      );

    ros::spin();
    return 0;
}
