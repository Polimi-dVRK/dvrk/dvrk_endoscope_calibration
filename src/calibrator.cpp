//
// Created by tibo on 12/07/17.
//

#include <cmath>
#include <opencv2/core/types.hpp>

#include <dvrk_common/math.hpp>
#include <dvrk_common/to_string.hpp>
#include <dvrk_common/opencv/parse.hpp>
#include <dvrk_common/ros/params.hpp>
#include <dvrk_common/ros/conversions/cv_ros.hpp>

#include "endoscope_calibration/calibrator.hpp"

/**
 * Constructor.
 *
 * Loads the node parameters from the launch file and initialises everything.
 */
Calibrator::Calibrator()
    : _node()
    , _pnode("~")
    , _stereo_camera(_node)
{
    // Set up cameras
    _stereo_camera.subscribe("left", "right");
    
    _left_roi_param_namespace = dvrk::getRequiredParam<std::string>(_pnode, "left_roi_namespace");
    _right_roi_param_namespace = dvrk::getRequiredParam<std::string>(_pnode, "right_roi_namespace");
    
    // Set up pedal event subscribers.
    auto dvrk_footpedals_ns = dvrk::getRequiredParam<std::string>(_pnode, "dvrk_footpedals_ns");

    auto camera_plus_topic = ros::names::append(dvrk_footpedals_ns, "camera_plus");
    _camera_plus_subscriber = _node.subscribe(camera_plus_topic, 1, &Calibrator::OnCameraPlus, this);

    auto camera_minus_topic = ros::names::append(dvrk_footpedals_ns, "camera_minus");
    _camera_minus_subscriber = _node.subscribe(camera_minus_topic, 1, &Calibrator::OnCameraMinus, this);

    auto coag_topic = ros::names::append(dvrk_footpedals_ns, "coag");
    _coag_subscriber = _node.subscribe(coag_topic, 1, &Calibrator::OnCoag, this);

    auto clutch_topic = ros::names::append(dvrk_footpedals_ns, "clutch");
    _clutch_subscriber = _node.subscribe(clutch_topic, 1, &Calibrator::OnClutch, this);

    // The ROI size must be set in the launch file
    auto roi_size_str = dvrk::getRequiredParam<std::string>(_pnode, "roi_size");
    if (auto parsed_roi = dvrk::parsePoint(roi_size_str)) {
        _roi_size = *parsed_roi;
    } else {
        ROS_FATAL(
            "Unable to parse region of interest of size. It should be specified as \"w, h\" where \"w\" ad \"h\" are "
                "positive integers."
        );
        ros::shutdown();
    }
}

/**
 * Main loop. This is where everything happens.
 */
void Calibrator::run() {
    // It looks like something isn't working the way I think it is and this value needs to be waaaaaay higher than
    // expected. In any case it doesn't work out to pixels/s.
    const float px_per_second = 150;

    _stereo_camera.waitForFirstImage();
    const auto& camera_info = _stereo_camera.getLeftCameraInfo();
    const unsigned int image_width = camera_info.width;
    const unsigned int image_height = camera_info.height;
    
    // If the ROI is larger than the image then we can't actually move it
    ROS_ASSERT_MSG(
        _roi_size.x <= image_width && _roi_size.y <= image_height,
        "The starting ROI cannot be larger than the image size"
    );
    
    // The maximum offset is the maximum amount that the ROI can be pushed in the x and y directions before it extends
    // outside of the image bounds. Pushing an out of bounds region of interest causes an error so we'd like to avoid
    // to avoid that if at all possible.
    cv::Point2f max_offset(
        static_cast<float>(std::floor(image_width - _roi_size.x)),
        static_cast<float>(std::floor(image_height - _roi_size.y))
    );
    
    // Initially we position the regions of interest such that the images are as close as possible.
    // This means that the // left image is pushed as far right as possible
    // FIXME: reset the y offets on both ROIs to `y_offset`
    double y_offset = std::floor(image_height / 2.0 - _roi_size.y / 2.0);
    cv::Rect2d left_roi(max_offset.x, y_offset, _roi_size.x, _roi_size.y);

    // And the right image is pushed as far left as possible
    cv::Rect2d right_roi(0, y_offset, _roi_size.x, _roi_size.y);
    
    setROIParam(left_roi, right_roi);

    ROS_INFO_STREAM("Initial left image ROI: " << left_roi);
    ROS_INFO_STREAM("Initial right image ROI: " << right_roi);

    // Having positioned the image we can now start the main loop
    ros::Rate loop_rate(100);

    while (ros::ok()) {
        ros::spinOnce();
        bool has_changed = false;

        // This is the amount that the image will move in this frame
        auto offset = px_per_second * loop_rate.cycleTime().toSec();
        // ROS_INFO_STREAM("Actual cycle time: " << loop_rate.cycleTime().toSec() << ", Expected: " << loop_rate.expectedCycleTime().toSec());

        // If both buttons controlling one axis are pressed then the actions cancel each other out
        if (_camera_plus_state == ButtonState::Pressed) {
            left_roi.y += offset;
            right_roi.y -= offset;
            has_changed = true;
        }

        if (_camera_minus_state == ButtonState::Pressed) {
            left_roi.y -= offset;
            right_roi.y += offset;
            has_changed = true;
        }

        if (_coag_state == ButtonState::Pressed) {
            left_roi.x -= offset;
            right_roi.x += offset;
            has_changed = true;
        }

        if (_clutch_state == ButtonState::Pressed) {
            left_roi.x += offset;
            right_roi.x -= offset;
            has_changed = true;
        }

        // only do anything if the buttons where pressed.
        if (has_changed) {
            left_roi.x = dvrk::clamp<double>(left_roi.x, 0.0f, max_offset.x);
            left_roi.y = dvrk::clamp<double>(left_roi.y, 0.0f, max_offset.y);

            right_roi.x = dvrk::clamp<double>(right_roi.x, 0.0f, max_offset.x);
            right_roi.y = dvrk::clamp<double>(right_roi.y, 0.0f, max_offset.y);

            setROIParam(left_roi, right_roi);
            
            ROS_INFO_STREAM(
                "" << "Left ROI: " << dvrk::to_string(left_roi).c_str()
                   << ", Right ROI: " << dvrk::to_string(right_roi).c_str()
            );
        }

        loop_rate.sleep();
    }

}


/**
 * Set the region of interest of the two cameras based on the two \c roi inputs.
 *
 * @param left_roi The region of interest to be set on the left camera
 * @param right_roi The region of interest to be set on the right camera
 */
void Calibrator::setROIParam(const cv::Rect2d &left_roi, const cv::Rect2d &right_roi)
{
    _node.setParam(ros::names::append(_left_roi_param_namespace, "x_offset"), left_roi.x);
    _node.setParam(ros::names::append(_left_roi_param_namespace, "y_offset"), left_roi.y);
    _node.setParam(ros::names::append(_left_roi_param_namespace, "width"), left_roi.width);
    _node.setParam(ros::names::append(_left_roi_param_namespace, "height"), left_roi.height);

    _node.setParam(ros::names::append(_right_roi_param_namespace, "x_offset"), right_roi.x);
    _node.setParam(ros::names::append(_right_roi_param_namespace, "y_offset"), right_roi.y);
    _node.setParam(ros::names::append(_right_roi_param_namespace, "width"), right_roi.width);
    _node.setParam(ros::names::append(_right_roi_param_namespace, "height"), right_roi.height);
}


/// Callback for messages on the \c CAMERA_PLUS topic
void Calibrator::OnCameraPlus(const sensor_msgs::Joy& msg) {
    _camera_plus_state = static_cast<ButtonState>(msg.buttons.at(0));
}


/// Callback for messages on the \c CAMERA_MINUS topic
void Calibrator::OnCameraMinus(const sensor_msgs::Joy& msg) {
    _camera_minus_state = static_cast<ButtonState>(msg.buttons.at(0));
}


/// Callback for messages on the \c COAG topic
void Calibrator::OnCoag(const sensor_msgs::Joy& msg) {
    _coag_state = static_cast<ButtonState>(msg.buttons.at(0));
}


/// Callback for messages on the \c CLUTCH topic
void Calibrator::OnClutch(const sensor_msgs::Joy& msg) {
    _clutch_state = static_cast<ButtonState>(msg.buttons.at(0));
}
